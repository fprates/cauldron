package org.quantic.cauldron.dbconnector;

import java.util.HashSet;
import java.util.Set;

import org.quantic.cauldron.application.Connector;
import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.application.DBSession;
import org.quantic.cauldron.datamodel.TypeContextEntry;

public class DummyConnector implements Connector {
    
    @Override
    public final void create(TypeContextEntry type) { }
    
    @Override
    public final Set<String> getDocuments() {
        return new HashSet<>();
    }
    
    @Override
    public final DBSession instance(Context context) {
        return new DummySession();
    }
}
