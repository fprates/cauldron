package org.quantic.cauldron.trace;

public class ProgramSignalException extends Exception {
    private static final long serialVersionUID = -481655637829286999L;
    public SignalData data;
    
    public ProgramSignalException(SignalData data) {
        this.data = data;
    }
}
