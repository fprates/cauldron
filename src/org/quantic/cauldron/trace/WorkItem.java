package org.quantic.cauldron.trace;

import java.util.HashSet;
import java.util.Set;

public class WorkItem {
    public String outputname;
    public int level;
    public WorkItem reference;
    public ProgramItem pitem;
    public Set<String> ctxdata;
    
    public WorkItem() {
        ctxdata = new HashSet<>();
    }
    
    public final void remove(String name) {
        ctxdata.remove(name);
    }
    
    @Override
    public final String toString() {
        if (pitem.oconn == null)
            return super.toString();
        if (pitem.oconn.facility == null)
            return pitem.oconn.toString();
        return new StringBuilder(pitem.oconn.parameter).
                append(" = ").append(pitem.toString()).toString();
    }
}

