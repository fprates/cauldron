package org.quantic.cauldron.trace;

import java.util.HashSet;
import java.util.Set;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.datamodel.DataContext;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.facility.FunctionCall;

public class CauldronProgram {
    private int id;
    public ProgramItem[] items;
    public Set<String> inputs;
    public ObjectConnection oconn;
    
    public CauldronProgram(int id) {
        inputs = new HashSet<>();
        this.id = id;
    }
    
    public final DataContextEntry execute(
            Context context, CauldronExecutionData execdata) throws Exception {
        ProgramItem item = null;
        DataContext datactx = context.datactx();

        execdata.ctxpool = new ContextPool();
        for (String key : datactx.instances())
            execdata.ctxpool.instance(datactx.get(key));
        
        for (execdata.i = items.length - 1; execdata.i >= 0; execdata.i--)
            try {
                item = executeItem(context, execdata);
            } catch (ProgramSignalException e) {
                return null;
            }
        
        if (item != null)
            initArgument(execdata.ctxpool, item);
        
        return datactx.get(oconn.name);
    }
    
    private final ProgramItem executeItem(
            Context context, CauldronExecutionData execdata) throws Exception {
        ProgramItem item = items[execdata.i];
        FunctionCall fnccall = context.function(item.function, item.taskdata);
        
        item.taskdata.compile(context);
        try {
            item.output = context.call(item.facility, fnccall);
        } catch (ProgramSignalException e) {
            execdata.signal = e.data;
            throw e;
        }
        prepareNextItem(context, item);
        
        return item;
    }
    
    public final int getId() {
        return id;
    }
    
    private final void initArgument(ContextPool ctxpool, ProgramItem item) {
        if (item.output != null)
            oconn = ctxpool.connectionInstance(item.output.getName());
    }
    
    private final void prepareNextItem(Context context, ProgramItem item) {
        if (item.source != null)
            items[item.reference].taskdata.set(item.source, item.output);
        if (item.output == null)
            context.log("return skipped.");
    }
}

