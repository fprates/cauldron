package org.quantic.cauldron.trace;

import java.util.Map;

public class TaskDataParameters {
    public WorkItem reference;
    public Map<String, String[]> input;
    public ObjectConnection oconn;
    public WorkItemData widata;
    public MakeSpec spec;
    
    public TaskDataParameters(WorkItem reference, ObjectConnection oconn) {
        this.reference = reference;
        this.oconn = oconn;
    }
}

