package org.quantic.cauldron.trace;

import java.util.HashSet;
import java.util.Set;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.application.target.Target;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.datamodel.TypeContextEntry;

public class ContextPool {
    public int resultlimit;
    public ObjectConnectionPool cpool;
    public Target target;
    
    public ContextPool() {
        cpool = new ObjectConnectionPool();
    }

    public final String connect(String facility, String function,
            String direction, String dname, String dtype) {
        String name = new StringBuilder(facility).append(".").
                append(function).append(":").append(dname).toString();
        ObjectConnection connection = connectionInstance(name);
        connection.facility = facility;
        connection.function = function;
        connection.parameter = dtype;
        connection.direction = direction;
        connectionCompile(name);
        return name;
    }
    
    private final void connectionCompile(String name) {
        cpool.compile(name);
    }
    
    public final ObjectConnection connectionInstance(String name) {
        return cpool.instance(name);
    }
    
    public final Set<ObjectConnection> getConnections(String name) {
        return cpool.get(name);
    }
    
    public final Set<String> getInstances(String name) {
        Set<String> instances;
        Set<ObjectConnection> conns = cpool.get(name);
        
        if (conns == null)
            return null;
        instances = null;
        for (ObjectConnection conn : conns) {
            if (conn.function != null)
                continue;
            if (instances == null)
                instances = new HashSet<>();
            instances.add(conn.name);
        }
        return instances;
    }
    
    public final String instance(
            Context context, String type, String name, Object value) {
        DataContextEntry object = context.datactx().instance(type, name);
        object.set(value);
        return instance(object);
    }

    public final String instance(DataContextEntry object) {
        InstanceParameters params = new InstanceParameters();
        TypeContextEntry type = object.getType();
        
        params.object = object;
        params.type = type.getPath();
        params.iname = object.getName();
        return instance(params);
    }

    public final String instance(InstanceParameters params) {
        ObjectConnection oconn = cpool.instance(params.iname);
        oconn.parameter = params.type;
        cpool.compile(params.iname);
        return params.iname;
    }
}
