package org.quantic.cauldron.trace;

import java.util.LinkedHashMap;
import java.util.Map;

import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.facility.FunctionParameters;

public class ProgramItem {
    public String facility, function, source;
    public FunctionParameters taskdata;
    public int id, reference;
    public ObjectConnection oconn;
    public Map<String, String> tinputs;
    public DataContextEntry output;
    
    public ProgramItem(String facility, String function, int reference, int id)
    {
        this.facility = facility;
        this.function = function;
        this.reference = reference;
        this.id = id;
        taskdata = new FunctionParameters();
        tinputs = new LinkedHashMap<>();
    }

    @Override
    public final String toString() {
        StringBuilder sb = new StringBuilder(oconn.facility).append(".").
                append(oconn.function).append("(");
        String value = null;
        for (String key : tinputs.keySet()) {
            if (value != null)
                sb.append(",");
            sb.append(value = tinputs.get(key));
        }
        return sb.append(")").toString();
    }
}
