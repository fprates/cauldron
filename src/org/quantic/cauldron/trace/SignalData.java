package org.quantic.cauldron.trace;

import java.util.HashMap;
import java.util.Map;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.datamodel.DataContextEntry;

public class SignalData {
    public String name, datatype;
    public Map<String, Object> data;
    
    public SignalData(String name) {
        this.name = name;
        data = new HashMap<>();
    }
    
    public final DataContextEntry get(Context context) {
        DataContextEntry data = context.datactx().
                instance(datatype, "signal_object");
        for (String key : this.data.keySet())
            data.set(key, this.data.get(key));
        return data;
    }
}
