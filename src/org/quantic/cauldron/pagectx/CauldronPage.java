package org.quantic.cauldron.pagectx;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CauldronPage {
    public PageSpec spec;
    public PageConfig config;
    public CauldronPageType type;
    public PageContext pagectx;
    public Set<String> output;
    public Map<String, String> input;
    
    public CauldronPage() {
        this(null, null);
    }
    
    public CauldronPage(PageSpec spec, PageConfig config) {
        this.spec = spec;
        this.config = config;
        output = new HashSet<>();
        input = new HashMap<>();
    }
    
    public final PageContext page() {
        return pagectx = new StandardPageContext();
    }
}
