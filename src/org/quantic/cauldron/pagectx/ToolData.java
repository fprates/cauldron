package org.quantic.cauldron.pagectx;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.quantic.cauldron.output.ToolFactory;
import org.quantic.cauldron.pagectx.PageSpecItem.TYPES;

public class ToolData {
	public String sh, name, model, style, parent, group, label, action, source;
	public String text, tag, subpage, indexitem, current, mask, image, nsdata;
	public String facility, mark;
	public boolean internallabel, submit, disabled, secret;
	public boolean invisible, required, focus, nolock, cancellable, absolute;
	public boolean noheader, singlemark;
	public int length, vlength, topline, step;
	public String[] groups, ordering;
	public Object[] textargs;
	public Object nsvalue;
	public PageSpecItem type;
	public Map<String, ToolData> items;
	public Map<String, Object> values;
	public Map<String, String> events, attributes, styles;
	public Set<String> validators, actions;
	public ToolFactory factory;
	
	public ToolData(PageSpecItem type) {
	    this(type, null);
	}
	
	public ToolData(PageSpecItem type, String name) {
	    this.type = type;
	    this.name = name;
	    items = new LinkedHashMap<>();
	    attributes = new HashMap<>();
	    actions = new LinkedHashSet<>();
	    events = new HashMap<>();
	    validators = new HashSet<>();
	    values = new LinkedHashMap<>();
	    styles = new HashMap<>();
	}
	
	public final void clear() {
	    model = null;
	    ordering = null;
	}
	
	public final ToolData clone(String prefix, ToolData parent)
	        throws Exception {
	    Object valuefrom;
	    ToolData data = new ToolData(type);
	    Class<?> _class = getClass();
	    
	    for (Field field : _class.getFields()) {
	        valuefrom = field.get(this);
	        field.set(data, valuefrom);
	    }
	    
	    data.name = String.format("%s_%s", prefix, data.name);
	    data.parent = (this.parent == null)? parent.name :
	        String.format("%s_%s", prefix, this.parent);
	    return data;
	}
	
	public final boolean contains(String name) {
	    return items.containsKey(name);
	}
	
	public final Map<String, ToolData> get() {
	    return items;
	}
	
	public final ToolData instance(String name) {
		return instance(null, name);
	}
	
	public final ToolData instance(TYPES type, String name) {
	    ToolData item;
	    
	    item = items.get(name);
	    if (item == null) {
	        item = new ToolData(null);
	        item.name = name;
	        if (type != null)
	        	item.type = new PageSpecItem(this.name, type, name);
	        if (name != null)
	            put(name, item);
	    }
	    return item;
	}
	
	private final void put(String name, ToolData item) {
	    items.put(name, item);
	}
	
	public final int size() {
		return items.size();
	}
	
	@Override
	public final String toString() {
		return new StringBuilder(type.toString()).
				append(": ").append(name).toString();
	}
}
