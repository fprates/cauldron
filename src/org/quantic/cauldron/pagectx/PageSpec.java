package org.quantic.cauldron.pagectx;

import java.util.Collection;

import org.quantic.cauldron.pagectx.PageSpecItem.TYPES;

public interface PageSpec {
    
    public abstract PageSpecItem get(String name);
    
    public abstract Collection<PageSpecItem> getItems();
    
    public abstract void put(String parent, TYPES type, String name);
    
    public abstract void run(PageContext pagectx);
    
    public abstract void run(PageContext pagectx, PageSpecItem item);
	
}

