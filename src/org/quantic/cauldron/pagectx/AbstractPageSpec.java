package org.quantic.cauldron.pagectx;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.quantic.cauldron.pagectx.PageSpecItem.TYPES;

public abstract class AbstractPageSpec implements PageSpec {
    protected String parent;
    private Map<String, PageSpecItem> items;
    private PageContext pagectx;
    
    public AbstractPageSpec() {
        items = new LinkedHashMap<>();
    }
    
    protected final void button(String parent, String name) {
        put(parent, PageSpecItem.TYPES.BUTTON, name);
    }
    
    protected final void component(
            PageSpecItem.TYPES type, String parent, String name) {
        put(parent, type, name);
    }
    
    protected final void container(String parent, String name) {
        put(parent, PageSpecItem.TYPES.STANDARD_CONTAINER, name);
    }
    
    protected final void dataform(String parent, String name) {
        put(parent, PageSpecItem.TYPES.DATA_FORM, name);
    }
    
    protected abstract void execute();
    
    protected final void expandbar(String parent, String name) {
        put(parent, PageSpecItem.TYPES.EXPAND_BAR, name);
    }
    
    protected final void fileupload(String parent, String name) {
        put(parent, PageSpecItem.TYPES.FILE_UPLOAD, name);
    }
    
    protected final void form(String name) {
    	put("view", PageSpecItem.TYPES.FORM, name);
    }
    
    protected final void frame(String parent, String name) {
        put(parent, PageSpecItem.TYPES.FRAME, name);
    }
    
    @Override
    public final PageSpecItem get(String name) {
        return items.get(name);
    }
    
    @Override
    public final Collection<PageSpecItem> getItems() {
        return items.values();
    }
    
    protected final void link(String parent, String name) {
        put(parent, PageSpecItem.TYPES.LINK, name);
    }
    
    protected final void listbox(String parent, String name) {
        put(parent, PageSpecItem.TYPES.LISTBOX, name);
    }
    
    protected final void message(String parent, String name) {
    	put(parent, PageSpecItem.TYPES.MESSAGE, name);
    }
    
    protected final void navcontrol(String parent) {
        put(parent, PageSpecItem.TYPES.PAGE_CONTROL, "navcontrol");
    }
    
    protected final void navcontrol(String parent, String name) {
        put(parent, PageSpecItem.TYPES.PAGE_CONTROL, name);
    }
    
    protected final void nodelist(String parent, String name) {
        put(parent, PageSpecItem.TYPES.NODE_LIST, name);
    }
    
    protected final void nodelistitem(String parent, String name) {
        put(parent, PageSpecItem.TYPES.NODE_LIST_ITEM, name);
    }
    
    protected final void parameter(String parent, String name) {
        put(parent, PageSpecItem.TYPES.PARAMETER, name);
    }
    
    protected final void printarea(String parent) {
        put(parent, PageSpecItem.TYPES.PRINT_AREA, "printarea");
    }
    
    @Override
    public final void put(String parent, TYPES type, String name) {
        PageContext parentpage;
        PageSpecItem item = new PageSpecItem(parent, type, name);
        if (this.parent == null) {
        	items.put(name, item);
        	return;
        }
        
        if ((parentpage = pagectx.getParent()) != null)
            parentpage.getSpec().put(parent, type, name);
        else
            pagectx.getSpec().put(parent, type, name);
    }
    
    protected final void reporttool(String parent, String name) {
        put(parent, PageSpecItem.TYPES.REPORT_TOOL, name);
    }
    
    protected final void radiobutton(String parent, String group, String name)
    {
    	String specname =
    			new StringBuilder(group).append(".").append(name).toString();
        put(parent, PageSpecItem.TYPES.RADIO_BUTTON, specname);
        pagectx.addSpecAlias(name, specname);
    }
    
    protected final void radiogroup(String parent, String name) {
        put(parent, PageSpecItem.TYPES.RADIO_GROUP, name);
    }
    
    @Override
    public final void run(PageContext pagectx) {
        run(pagectx, null);
    }
    
    @Override
    public final void run(PageContext pagectx, PageSpecItem item) {
        this.pagectx = pagectx;
        items.clear();
        if (item != null) {
            parent = item.getName();
            items.put(parent, item);
        }
        execute();
    }
    
    protected final void spec(String parent, String name) {
        PageSpecItem specitem = (this.parent == null)?
                items.get(parent) : pagectx.getSpec().get(parent);
        spec(pagectx, specitem, name);
    }
    
    private final void spec(
            PageContext page, PageSpecItem parent, String name) {
        PageSpec extspec;
        PageContext child = page.getChild(name);
        
        if (child == null) {
            for (String key : page.getChildren())
                spec(page.getChild(key), parent, name);
            return;
        }
        
        extspec = child.getSpec();
        if (page.isSubPage(name) || (extspec == null))
            return;
        extspec.run(child, parent);
        for (PageSpecItem item : extspec.getItems())
            items.put(item.getName(), item);
    }
    
    protected final void tabbedpane(String parent, String name) {
        put(parent, PageSpecItem.TYPES.TABBED_PANE, name);
    }
    
    protected final void tabbedpaneitem(String parent, String name) {
        put(parent, PageSpecItem.TYPES.TABBED_PANE_ITEM, name);
    }
    
    protected final void tabletool(String parent, String name) {
        put(parent, PageSpecItem.TYPES.TABLE_TOOL, name);
    }
    
    protected final void text(String parent, String name) {
        put(parent, PageSpecItem.TYPES.TEXT, name);
    }
    
    protected final void texteditor(String parent, String name) {
        put(parent, PageSpecItem.TYPES.TEXT_EDITOR, name);
    }
    
    protected final void textfield(String parent, String name) {
        put(parent, PageSpecItem.TYPES.TEXT_FIELD, name);
    }
    
    protected final void tiles(String parent, String name) {
        put(parent, PageSpecItem.TYPES.TILES, name);
    }
    
    protected final void treetool(String parent, String name) {
        put(parent, PageSpecItem.TYPES.TREE_TOOL, name);
    }
    
    protected final void virtualcontrol(String parent, String name) {
        put(parent, PageSpecItem.TYPES.VIRTUAL_CONTROL, name);
    }
}

