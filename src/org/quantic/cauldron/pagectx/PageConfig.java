package org.quantic.cauldron.pagectx;

import org.quantic.cauldron.application.Context;

public interface PageConfig {
    
    public abstract void run(PageContext pagectx, Context context);
    
    public abstract void run(
            PageContext pagectx, Context context, String prefix);
	
}

