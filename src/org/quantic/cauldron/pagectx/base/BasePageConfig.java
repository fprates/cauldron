package org.quantic.cauldron.pagectx.base;

import java.util.LinkedHashMap;
import java.util.Map;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.pagectx.AbstractPageConfig;
import org.quantic.cauldron.pagectx.PageContext;
import org.quantic.cauldron.pagectx.ToolData;

public class BasePageConfig extends AbstractPageConfig<Context> {
	private static final Map<String, String[][]> H_ITEMS;
	
	static {
		String[][] item;
		H_ITEMS = new LinkedHashMap<>();
		
		item = new String[][] {
			{"meta"},
			{"http-equiv", "Content-Type"},
			{"content", "text/html; charset=utf-8"} };
		H_ITEMS.put("meta-01", item);
		item = new String[][] {
			{"meta"},
			{"name", "viewport"},
			{"content",
				"width=device-width, initial-scale=1, shrink-to-fit=no"} };
		H_ITEMS.put("meta-02", item);
		item = new String[][] {
			{"link"},
			{"rel", "stylesheet"},
			{"href", "estilo/style.css"} };
		H_ITEMS.put("link-01", item);
		item = new String[][] {
			{"link"},
			{"rel", "stylesheet"},
			{"href", "node_modules/bootstrap/dist/css/bootstrap.min.css"} };
		H_ITEMS.put("link-02", item);
		item = new String[][] {
			{"link"},
			{"href",
				"https://fonts.googleapis.com/css?family=Roboto&display=swap"},
			{"rel", "stylesheet"} };
		H_ITEMS.put("link-03", item);
		item = new String[][] {
			{"link"},
			{"rel", "icon"},
			{"href", "imgs/favicon.ico"},
			{"type", "image/x-icon"},
			{"sizes", "16x16"} };
		H_ITEMS.put("link-04", item);
		item = new String[][] {
			{"script", ""},
			{"type", "text/javascript"},
			{"src", "sigmajs/script.js"} };
		H_ITEMS.put("script-01", item);
		item = new String[][] {
			{"script", ""},
			{"type", "text/javascript"},
			{"src", "node_modules/jquery/dist/jquery.js"} };
		H_ITEMS.put("script-02", item);
		item = new String[][] {
			{"script", ""},
			{"type", "text/javascript"},
			{"src", "node_modules/popper.js/dist/umd/popper.js"} };
		H_ITEMS.put("script-03", item);
		item = new String[][] {
			{"script", ""},
			{"type", "text/javascript"},
			{"src", "node_modules/bootstrap/dist/js/bootstrap.js"} };
		H_ITEMS.put("script-04", item);
		H_ITEMS.put("style", new String[][] {
			{"style", "body {font-family: 'Roboto', sans-serif;}"} });
	}

	@Override
	protected void execute(PageContext pagectx, Context context) {
		ToolData data;
		
		pagectx.setResponseComponent("fullcontent");
		pagectx.getHead().putAll(H_ITEMS);
		
		data = getTool("main");
		data.attributes.put("style", "width:100%;height:100%;");
		
		data = getTool("fullcontent");
		data.attributes.put("font-family", "'Roboto', sans-serif;");
		
		data = getTool("message");
		data.tag = "div";
		data.style = "text-center full fixed-bottom";
		data.styles.put(
				"error", "error-message text-center full fixed-bottom");
		data.styles.put(
				"status", "status-message text-center full fixed-bottom");
		data.styles.put(
				"warning", "warning-message text-center full fixed-bottom");
		config("custom");
	}
	
}
