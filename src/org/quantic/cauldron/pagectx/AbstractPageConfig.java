package org.quantic.cauldron.pagectx;

import org.quantic.cauldron.application.CauldronException;
import org.quantic.cauldron.application.Context;

public abstract class AbstractPageConfig<C extends Context>
		implements PageConfig {
	private C context;
    private PageContext pagectx;
    
    /**
     * 
     * @param viewconfig
     */
    protected final void config(String name) {
        config(pagectx, name);
    }
    
    private final void config(PageContext page, String name) {
        PageConfig extconfig;
        PageContext child = page.getChild(name);
        
        if (child == null) {
            for (String key : page.getChildren())
                config(page.getChild(key), name);
            return;
        }
        
        extconfig = child.getConfig();
        if (page.isSubPage(name) || (extconfig == null))
            return;
        extconfig.run(child, context);
    }
    
    /**
     * 
     * @param context
     */
    protected abstract void execute(PageContext pagectx, C context);
    
    private final ToolData get(PageContext pagectx, String name) {
    	PageContext parent = pagectx.getParent();
    	ToolData tooldata = pagectx.get(name);
    	return ((tooldata == null) && (parent != null))?
    			get(parent, name) : tooldata;
    }
    
    protected final ToolData getTool(String name) {
        PageContext parent;
        ToolData tooldata = pagectx.get(name);
        if (tooldata != null)
        	return tooldata;
        for (String key : pagectx.getChildren())
            if ((tooldata = pagectx.getChild(key).instance(name)) != null)
            	return tooldata;
        parent = pagectx.getParent();
        if ((parent != null) && ((tooldata = get(parent, name)) != null))
            return tooldata;
        throw new CauldronException("element %s undefined.", name);
    }
    
    /*
     * (não-Javadoc)
     * @see org.iocaste.appbuilder.common.ViewConfig#run(
     *    org.iocaste.appbuilder.common.PageBuilderContext)
     */
	@Override
    @SuppressWarnings("unchecked")
    public final void run(PageContext pagectx, Context context) {
        this.context = (C)context;
        this.pagectx = pagectx;
        execute(pagectx, this.context);
    }
    
    /*
     * (non-Javadoc)
     * @see org.iocaste.appbuilder.common.ViewConfig#run(
     *    org.iocaste.appbuilder.common.PageBuilderContext, java.lang.String)
     */
	@Override
    @SuppressWarnings("unchecked")
    public final void run(PageContext pagectx, Context context, String prefix) {
        this.context = (C)context;
        this.pagectx = pagectx;
        execute(pagectx, this.context);
    }
	
}

