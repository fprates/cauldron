package org.quantic.cauldron.facilities.pageactions;

import org.quantic.cauldron.application.ApplicationContext;
import org.quantic.cauldron.facility.AbstractFunction;
import org.quantic.cauldron.facility.FunctionConfig;
import org.quantic.cauldron.facility.Session;

public class PageContextGet extends AbstractFunction {
    private String facility;
    private FunctionConfig config;
    
    public PageContextGet(String facility) {
        super("pagectx_get");
        this.facility = facility;
        config(config);
    }

    @Override
    protected void config(FunctionConfig config) {
        if (facility == null) {
            this.config = config;
            return;
        }
        config.input(facility.concat(".string"), "page_name");
        config.output(facility.concat(".page_context"));
    }

    @Override
    protected void execute(Session session) throws Exception {
        ApplicationContext context = session.context();
        session.output().set(context.pages.get(session.get("page_name")));
    }
    
}