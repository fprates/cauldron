package org.quantic.cauldron.facilities.pageactions;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.facility.AbstractFunction;
import org.quantic.cauldron.facility.FunctionCall;
import org.quantic.cauldron.facility.FunctionConfig;
import org.quantic.cauldron.facility.Session;
import org.quantic.cauldron.pagectx.PageContext;
import org.quantic.cauldron.pagectx.ToolData;

public class TabChange extends AbstractFunction {
    private String facility;
    private FunctionConfig config;
    
    public TabChange(String facility) {
        super("tab_change");
        this.facility = facility;
        config(config);
    }

    @Override
    protected void config(FunctionConfig config) {
    	if (facility != null)
            config.input(facility.concat(".page_context"), "pagectx");
		this.config = config;
    }

    @Override
    protected void execute(Session session) throws Exception {
        String action = session.geto("pagectx").getst("action");
        Context context = session.context();
        String page = context.getPage();
        FunctionCall pagectxget = context.function("pagectx_get", page);
        PageContext pagectx = (PageContext)context.call(facility, pagectxget);
        ToolData tabpane = pagectx.instance(action);
        pagectx.instance(tabpane.parent).current = action;
    }
	
}
