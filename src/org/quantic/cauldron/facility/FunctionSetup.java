package org.quantic.cauldron.facility;

import org.quantic.cauldron.facility.FunctionConfig;

public interface FunctionSetup {
	
	public abstract FunctionExecutable setup(FunctionConfig config);
	
}

