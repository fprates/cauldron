package org.quantic.cauldron.facility;

public interface FunctionConfig {
    
    public abstract FunctionDetails get();

    public abstract void input(String type, String name);
    
    public abstract void output(String type);
    
    public abstract void splitOutput(boolean split);
}
