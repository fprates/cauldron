package org.quantic.cauldron.facility;

public class FunctionNotFoundException extends Exception {
    private static final long serialVersionUID = -4331309861879571361L;

    public FunctionNotFoundException(String function) {
        super(String.format("%s is an invalid function", function));
    }
}
