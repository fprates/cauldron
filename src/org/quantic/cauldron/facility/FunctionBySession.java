package org.quantic.cauldron.facility;

public interface FunctionBySession {
    
    public abstract Function instance();
    
}
