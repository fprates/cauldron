package org.quantic.cauldron.facility;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.datamodel.DataContextEntry;

public abstract class AbstractFunction implements Function {
    private String name;
    private FunctionDetails metadata;
    
    public AbstractFunction(String name) {
        this.name = name;
        config();
    }
    
    private final void config() {
        FunctionConfig fconfig = new StandardFunctionConfig();
        config(fconfig);
        metadata = fconfig.get();
    }
    
    protected abstract void config(FunctionConfig config);
    
    protected abstract void execute(Session session) throws Exception;
    
    @Override
    public final FunctionDetails getMetaData() {
        return metadata;
    }
    
    @Override
    public final String getName() {
        return name;
    }
    
    @Override
    public final DataContextEntry run(Context context, FunctionCall args)
            throws Exception {
        FunctionParameters taskdata = (FunctionParameters)args.input;
        SessionContext sessionctx = new SessionContext(context, taskdata);
        Session session = new Session(args.context, sessionctx);
        
        context.log("starting %s()...", name);
        sessionctx.error(args.retex);
        
        try {
            taskdata.splitoutput = getMetaData().splitoutput;
            execute(session);
            context.log("%s() finished successfully.", name);
        } catch (Exception e) {
            sessionctx.context().log(e);
            context.log("%s() finished with error.", name);
            throw e;
        }
        
        args.message = session.getMessage();
        return session.output();
    }
}
