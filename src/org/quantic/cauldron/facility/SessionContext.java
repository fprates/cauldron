package org.quantic.cauldron.facility;

import org.quantic.cauldron.application.Context;

public class SessionContext implements FunctionSession {
    private Context context;
    private FunctionParameters input;
    private Exception e;
    
    public SessionContext(Context context, FunctionParameters input) {
        this.context = context;
        this.input = input;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <C extends Context> C context() {
        return (C)context;
    }

    @Override
    public final void error(Exception e) {
        this.e = e;
    }
    
    @Override
    public final Exception getError() {
        return e;
    }
    
    @Override
    public final FunctionParameters input() {
        return input;
    }
}
