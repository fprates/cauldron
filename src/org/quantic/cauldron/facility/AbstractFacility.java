package org.quantic.cauldron.facility;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.datamodel.DataContextEntry;

public abstract class AbstractFacility implements Facility {
    private String name;
    private Map<String, Function> functions;
    private Context context;
    
    public AbstractFacility(Context context, String name) {
        this.context = context;
    	this.name = name;
    	functions = new HashMap<>();
    }
    
    @Override
    public final void add(Function function) {
    	functions.put(function.getName(), function);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final <C extends Context> C getContext() {
    	return (C)context;
    }
    
    @Override
    public final FunctionDetails getFunctionDetails(String function) {
    	return functions.get(function).getMetaData();
    }
    
    @Override
    public final Set<String> getFunctions() {
        return functions.keySet();
    }
    
    @Override
    public final String getName() {
        return name;
    }
    
    @Override
    public final DataContextEntry run(FunctionCall fnccall) throws Exception {
        Function function = functions.get(fnccall.function);
        if (function == null)
            throw new FunctionNotFoundException(fnccall.function);
        return function.run(getContext(), fnccall);
    }
}
