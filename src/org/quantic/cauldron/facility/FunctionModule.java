package org.quantic.cauldron.facility;
import org.quantic.cauldron.facility.AbstractFunction;
import org.quantic.cauldron.facility.FunctionConfig;
import org.quantic.cauldron.facility.Session;

public class FunctionModule extends AbstractFunction {
    private FunctionExecutable exec;
    private FunctionConfig config;
    
	public FunctionModule(String name, FunctionSetup setup) {
		super(name);
		exec = setup.setup(config);
	}

	@Override
	protected void config(FunctionConfig config) {
	    this.config = config;
	}

	@Override
	protected void execute(Session session) throws Exception {
	    exec.execute(session);
	}
	
}

