package org.quantic.cauldron.facility;

import org.quantic.cauldron.facility.Session;

public interface FunctionExecutable {

	public abstract void execute(Session session) throws Exception;
	
}

