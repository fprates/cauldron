package org.quantic.cauldron.facility;

public final class StandardFunctionConfig implements FunctionConfig {
    private FunctionDetails parameters;

    public StandardFunctionConfig() {
        parameters = new FunctionDetails();
    }
    
    @Override
    public final FunctionDetails get() {
        return parameters;
    }
    
    @Override
    public final void input(String type, String name) {
        parameters.add(name, type);
    }
    
    @Override
    public final void output(String type) {
        parameters.output = type;
    }
    
    @Override
    public final void splitOutput(boolean split) {
        parameters.splitoutput = split;
    }
}
