package org.quantic.cauldron.facility;

public class Message {
	public enum TYPE {
		ERROR,
		WARNING,
		STATUS
	};
	
	public TYPE type;
	public String text;
	public Object[] args;
	
	public Message(TYPE type, String text, Object... args) {
		this.type = type;
		this.text = text;
		this.args = args;
	}
}