package org.quantic.cauldron.output;

import java.text.DateFormat;
import java.util.HashMap;
import java.util.Map;

import org.quantic.cauldron.application.CauldronException;
import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.datamodel.TypeContextEntry;
import org.quantic.cauldron.pagectx.ToolData;

public abstract class AbstractToolFactory implements ToolFactory {
    private String name;
    private Map<String, Conversion> conversions;
    private DateFormat dtformat;
    
    public AbstractToolFactory(String name) {
    	this.name = name;
    	conversions = new HashMap<>();
    	conversions.put("date", (c, v)->date(c, v));
    }
    
    private final String date(ContextEntry ctxentry, Object value) {
        if (dtformat == null)
            dtformat = DateFormat.getDateInstance(
                    DateFormat.MEDIUM, ctxentry.locale);
        return (value == null)? "" : dtformat.format(value);
    }
    
    @Override
    public final void execute(ContextEntry config, ToolData data, String prefix)
            throws Exception {
    	if (data.tag == null)
    		data.tag = name;
    	if (data.style != null)
    		data.attributes.put("class", data.style);
    	if (data.name != null)
    		data.attributes.put("id", data.name);
    	generate(config, data, prefix);
    }
    
    protected abstract void generate(
    		ContextEntry config, ToolData data, String prefix) throws Exception;
    
    public final TypeContextEntry getType(
    		ContextEntry ctxentry, ToolData widget) throws Exception {
        Context context;
        TypeContextEntry type;
        
        if (widget.model == null)
            throw new CauldronException("type for form '%s' not specified.",
                    widget.name);
        
        context = (widget.facility == null)? ctxentry.context :
            ctxentry.context.getFacility(widget.facility).getContext();
        if ((type = context.typectx().get(widget.model)) == null)
            throw new CauldronException(
                "invalid type '%s' for form '%s'.", widget.model, widget.name);
        return type;
    }
    
    protected final String toString(ContextEntry ctxentry, String text) {
        if ((text == null) || (ctxentry.locale == null))
            return null;
        String translation = ctxentry.pagectx.
                getMessages(ctxentry.locale.toString()).get(text);
        return (translation == null)? text : translation;
    }
    
    protected final String toString(
            ContextEntry config, DataContextEntry data) throws Exception {
        return toString(config, data.getType(), data.getName());
    }
    
    protected final String toString(
            ContextEntry config, ToolData data) throws Exception {
        TypeContextEntry type = (data.model == null)?
                null : getType(config, data);
        return toString(config, type, data.name);
    }
    
    private final String toString(ContextEntry config,
            TypeContextEntry type, String name) throws Exception {
        Conversion conversion;
        DataContextEntry entry = config.context.datactx().get(name);
        Object value = (entry == null)? null : entry.get();
        if (type == null)
            return (value == null)? "" : value.toString();
        conversion = conversions.get(type.getName());
        if (conversion == null)
            return (value == null)? "" : value.toString();
        return conversion.toString(config, value);
    }
}

interface Conversion {
    
    public abstract String toString(ContextEntry ctxentry, Object value);
}
