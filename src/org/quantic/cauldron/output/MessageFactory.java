package org.quantic.cauldron.output;

import java.util.HashMap;
import java.util.Map;

import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.facility.Message;
import org.quantic.cauldron.facility.Message.TYPE;
import org.quantic.cauldron.pagectx.ToolData;

public class MessageFactory extends AbstractToolFactory {
	private static final Map<Message.TYPE, String> STYLES;
	
	static {
		STYLES = new HashMap<>();
		STYLES.put(TYPE.ERROR, "error");
		STYLES.put(TYPE.STATUS, "status");
		STYLES.put(TYPE.WARNING, "warning");
	}
	
	public MessageFactory() {
		super("div");
	}
	
	@Override
	protected void generate(ContextEntry config, ToolData data, String prefix) {
		if ((config.fncret == null) || (config.fncret.message == null))
			return;
		data.style = data.styles.get(STYLES.get(config.fncret.message.type));
		data.text = String.format(
				config.fncret.message.text, config.fncret.message.args);
	}
}
