package org.quantic.cauldron.output;

import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.pagectx.ToolData;

public class TextFactory extends AbstractToolFactory {

	public TextFactory() {
		super("p");
	}
	
	@Override
	protected void generate(ContextEntry config, ToolData data, String prefix)
	        throws Exception {
        data.text = (data.model != null)?
            toString(config, data) : toString(config, data.text);
	}
}
