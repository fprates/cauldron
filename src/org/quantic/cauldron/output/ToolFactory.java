package org.quantic.cauldron.output;

import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.pagectx.ToolData;

public interface ToolFactory {
	
	public abstract void execute(
			ContextEntry config, ToolData data, String prefix) throws Exception;
}
