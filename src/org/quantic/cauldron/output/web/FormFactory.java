package org.quantic.cauldron.output.web;

import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.output.AbstractToolFactory;
import org.quantic.cauldron.pagectx.ToolData;

public class FormFactory extends AbstractToolFactory {

	public FormFactory() {
		super("form");
	}
	
	@Override
	protected void generate(ContextEntry config, ToolData data, String prefix) {
		config.currentform = data.name;
		config.currentaction = data.action;
		data.attributes.put("method", "post");
	}
}
