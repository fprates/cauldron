package org.quantic.cauldron.output;

import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.datamodel.DataContext;
import org.quantic.cauldron.datamodel.TypeContextEntry;
import org.quantic.cauldron.pagectx.PageSpecItem;
import org.quantic.cauldron.pagectx.PageSpecItem.TYPES;
import org.quantic.cauldron.pagectx.ToolData;

public class DataFormFactory extends AbstractToolFactory {
    
	public DataFormFactory() {
		super("div");
	}
	
	private final ToolData builditem(ContextEntry config, String parent,
			ToolData data, String field, String suffix, TYPES type) {
		String fieldname = (suffix == null)?
				field : field.concat("_").concat(suffix);
    	String name = data.name.concat(".").concat(fieldname);
    	PageSpecItem specitem = new PageSpecItem(parent, type, name);
		ToolData item = data.items.get(fieldname);
		
		if (item == null)
			return config.pagectx.instance(specitem);
		
		item.name = name;
		item.type = specitem;
		item.parent = parent;
		data.instance(parent).items.put(name, item);
		data.items.remove(fieldname);
		
		return item;
	}
	
    @Override
    protected final void generate(ContextEntry ctxentry,
            ToolData widget, String prefix) throws Exception {
    	String itemname, typename;
    	ToolData item;
    	TypeContextEntry type = getType(ctxentry, widget);
    	DataContext datactx = ctxentry.context.datactx();
    	
    	if (datactx.get(widget.name) == null)
    		datactx.instance(type, widget.name);
    	
    	typename = type.getName();
    	for (String key : type.getItems()) {
    		item = builditem(ctxentry,
    				widget.name, widget, key, "item", TYPES.STANDARD_CONTAINER);
    		itemname = item.name;
    		item.style = widget.styles.get("item");
    		
    		item = builditem(
    				ctxentry, itemname, widget, key, "text", TYPES.TEXT);
    		item.style = widget.styles.get("item_text");
    		if (widget.internallabel)
    			item.text = "";
    		
    		item = builditem(
    				ctxentry, itemname, widget, key, null, TYPES.TEXT_FIELD);
    		item.style = widget.styles.get("item_input");
    		if (widget.internallabel)
    			item.attributes.put(
    			        "placeholder", toString(ctxentry, item.name));
    		item.facility = widget.facility;
    		item.model = new StringBuilder(typename).
    		        append(".").append(key).toString();
    	}
    }
}
