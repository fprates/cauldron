package org.quantic.cauldron.output;

import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.pagectx.ToolData;

public class FileUploadFactory extends AbstractToolFactory {

	public FileUploadFactory() {
		super("input");
	}
	
	@Override
	protected void generate(ContextEntry config, ToolData data, String prefix)
	        throws Exception {
		data.attributes.put("name", data.name);
		data.attributes.put("type", "file");
		config.pagectx.addInput(data.name);
	}
}
