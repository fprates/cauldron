package org.quantic.cauldron.application;

import java.util.HashMap;
import java.util.Map;

import org.quantic.cauldron.datamodel.Operand;

public class Operations {
    public static final byte PLUS = 0;
    public static final byte TIMES = 1;
    public static final byte EQUALS = 2;
    public static final byte SUB = 3;
    public static final byte DIV = 4;
    public static final Map<Byte, CalculateItem> citems;

    static {
        citems = new HashMap<>();
        new CalculateItem(citems, PLUS, "+",
                (cop, op)->cop.factor += op.factor);
        new CalculateItem(citems, TIMES, "*",
                (cop, op)->cop.factor *= op.factor);
        new CalculateItem(citems, EQUALS, "=",
                (cop, op)->{});
        new CalculateItem(citems, DIV, "/",
                (cop, op)->cop.factor /= op.factor);
        new CalculateItem(citems, SUB, "-",
                (cop, op)->cop.factor -= op.factor);
    }
    
    public final Operand add(Operand a, Operand b) {
        return exec(PLUS, a, b);
    }
    
    private final void addop(Operand acc, Operand op) {
        Operand temp;
        String name = (op.name == null)? "" : op.name;
        name = new StringBuilder(name).append("^").append(op.exp).toString();
        temp = acc.operands.get(name);
        if (temp == null)
            acc.operands.put(name, new Operand(op.factor, null, op.exp));
        else
            citems.get(acc.operation).op.calculate(temp, op);
    }
    
    public final Operand div(Operand a, Operand b) {
        return exec(DIV, a, b);
    }
    
    public final Operand equals(Operand a, Operand b) {
        return exec(EQUALS, a, b);
    }
    
    private final Operand exec(byte operation, Operand a, Operand b) {
        Operand result = new Operand();
        
        result.operation = operation;
        for (Operand op : new Operand[] {a, b})
            if (op.operands.size() > 0)
                for (String key : op.operands.keySet())
                    addop(result, op.operands.get(key));
            else
                addop(result, op);
        return result;
    }
    
    public static final double getFactor(Operand parent) {
        return (parent.operands.size() == 0)?
                parent.factor : getFactor(parent.operands.get("^1.0"));
    }
    
    public final Operand mul(Operand a, Operand b) {
        return exec(TIMES, a, b);
    }
    
    public final Operand sub(Operand a, Operand b) {
        return exec(SUB, a, b);
    }
}

interface CalculateOperation {
    
    public abstract void calculate(Operand coperand, Operand op);
}

class CalculateItem {
    public CalculateOperation op;
    public String signal;
    
    public CalculateItem(
            Map<Byte, CalculateItem> operations,
            byte operation,
            String signal,
            CalculateOperation op) {
        operations.put(operation, this);
        this.signal = signal;
        this.op = op;
    }
}
