package org.quantic.cauldron.application;

import java.util.ArrayList;
import java.util.List;

import org.quantic.cauldron.application.target.Rule;

public class EntryRule {
    private Rule rule;
    private List<EntryRule> rules;
    
    public EntryRule(List<EntryRule> rules, Rule rule) {
        this.rule = rule;
        this.rules = new ArrayList<>();
        rules.add(this);
    }
    
    public final EntryRule add(Rule rule) {
        return new EntryRule(rules, rule);
    }
    
    public final Rule getRule() {
        return rule;
    }
}
