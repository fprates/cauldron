package org.quantic.cauldron.application;

import java.util.Date;
import java.util.Set;

import org.quantic.cauldron.datamodel.DataContext;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.datamodel.TypeContext;
import org.quantic.cauldron.facility.Facility;
import org.quantic.cauldron.facility.FunctionCall;

public class CauldronContext implements Context {
    public static final byte BACK = 1;
    public static final byte HOME = 2;
    private FunctionCall logcall;
    private String faclog, fnclog, page;
    private TypeContext typectx;
    private DataContext datactx;
    private byte pagenav;
    private SharedContext sharedctx;
    
    public CauldronContext(SharedContext sharedctx, String facility) {
        typectx = new TypeContext(sharedctx, facility);
        datactx = new DataContext(sharedctx);
        page = "main";
        this.sharedctx = sharedctx;
    }
    
    @Override
    public final void back() {
    	pagenav = BACK;
    }
    
    @Override
    public final DataContextEntry call(String facname, FunctionCall fnccall)
    		throws Exception {
    	Facility facility = sharedctx.get(facname);
    	if (facility == null)
    	    throw new CauldronException("%s facility not registered.", facname);
    	return facility.run(fnccall);
    }
    
    @Override
    public final DataContext datactx() {
    	return datactx;
    }
    
    @Override
    public final FunctionCall function(String name, Object parameters) {
        return new FunctionCall(this, name, parameters);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final <C extends Context> C getContext(String factory) {
    	return (C)sharedctx.get(factory).getContext();
    }
    
    @Override
    public final Set<String> getFacilities() {
        return sharedctx.keySet();
    }
    
    @Override
    public final Facility getFacility(String name) {
    	return sharedctx.get(name);
    }
    
    @Override
    public final String getPage() {
    	return page;
    }
    
    @Override
    public final byte getPageNavigation() {
        return pagenav;
    }
    
    @Override
    public final void home() {
        pagenav = HOME;
    }
    
    @Override
    public final void log(Exception e) {
    	log(e.toString());
    	for (StackTraceElement ste : e.getStackTrace())
    		log(ste.toString());
    }
    
    @Override
    public final void log(String text, Object... args) {
    	Object[] fncargs;
    	
    	if (faclog == null)
    	    return;
    	logcall.function = fnclog;
    	fncargs = (Object[])logcall.input;
    	fncargs[0] = new Date();
    	fncargs[1] = text;
    	fncargs[2] = args;
    	try {
    	    call(faclog, logcall);
    	} catch (Exception e) {
    	    throw new RuntimeException(e);
    	}
    }
    
    @Override
    public final void reset() {
    	pagenav = 0;
    }
    
    @Override
    public final void setPage(String name) {
        if (name == null)
            throw new CauldronException("can't set current page to null.");
        this.page = name;
    }
    
    @Override
    public final void setLogTarget(String facility, String function) {
    	faclog = facility;
    	fnclog = function;
    }
    
    @Override
    public final TypeContext typectx() {
    	return typectx;
    }
}
