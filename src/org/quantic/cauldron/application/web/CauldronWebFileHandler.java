package org.quantic.cauldron.application.web;

import java.util.Map;

import org.quantic.cauldron.application.CauldronFileHandler;
import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.pagectx.ToolData;

public class CauldronWebFileHandler implements CauldronFileHandler {
    
    @Override
    public final void run(
            ContextEntry ctxentry, Map<String, ?> files, String key) {
        String filename;
        ToolData tooldata;
        FileStatus filestatus = (FileStatus)files.get(key);
        if ((filestatus == null) || !filestatus.file)
            return;
        
        tooldata = ctxentry.pagectx.instance(key);
        filename = filestatus.item.getName();
        if (filename.equals("")) {
            ctxentry.context.datactx().get(key).set(null);
            tooldata.values.remove("filename");
            tooldata.values.remove("content-type");
            tooldata.values.remove("error", "empty_filename");
            return;
        }
        
        if (tooldata.disabled)
            return;
        ctxentry.context.datactx().get(key).set(filestatus.item.get());
        tooldata.values.put("filename", filename);
        tooldata.values.put("content-type",
                filestatus.item.getContentType());
        tooldata.values.put("error", null);
        
    }
}
