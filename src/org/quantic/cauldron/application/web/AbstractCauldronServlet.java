package org.quantic.cauldron.application.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.quantic.cauldron.application.Application;
import org.quantic.cauldron.application.ApplicationContext;
import org.quantic.cauldron.application.Connector;
import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.application.GetString;
import org.quantic.cauldron.application.ProcessInput;
import org.quantic.cauldron.application.SharedContext;
import org.quantic.cauldron.facilities.pageactions.Back;
import org.quantic.cauldron.facilities.pageactions.Home;
import org.quantic.cauldron.facilities.pageactions.TabChange;

public abstract class AbstractCauldronServlet extends HttpServlet {
    private static final long serialVersionUID = 9217279031454282976L;
//    private TargetDetermination[] targetdet;
    private Application application;
    
    public AbstractCauldronServlet(String name) {
        SharedContext sharedctx = new SharedContext();
        ApplicationContext context = new ApplicationContext(sharedctx, name);
        
        application = new WebApplication(context, sharedctx, name);
        application.add(()->new Back());
        application.add(()->new Home());
        application.add(()->new TabChange(name));
        application.init((a)->init(a));
        
//        targetdet = new TargetDetermination[] {
//                (c, h)->directDetermination(c, h),
//                (c, h)->targetDetermination(c, h)
//        };
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
    		throws ServletException, IOException {
    	try {
    		run(req, resp, true);
    	} catch (Exception e) {
    		throw new ServletException(e);
    	}
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
    		throws ServletException, IOException {
    	try {
    		run(req, resp, false);
    	} catch (Exception e) {
    		throw new ServletException(e);
    	}
    }
    
    private final Map<String, String[]> extract(Map<String, FileStatus> files,
            HttpServletRequest req) throws Exception {
        ServletFileUpload fileupload;
        
        files.clear();
        if (!ServletFileUpload.isMultipartContent(req))
            return req.getParameterMap();
        fileupload = new ServletFileUpload(new DiskFileItemFactory());
        for (FileItem fileitem : (List<FileItem>)fileupload.parseRequest(req))
            files.put(fileitem.getFieldName(), new FileStatus(fileitem));
        return processMultipartContent(files);
    }
    
    protected abstract void init(Application application);
    
    private final void print(HttpServletResponse resp, String content)
            throws Exception {
        PrintWriter writer;
        
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        writer = resp.getWriter();
        writer.print(content);
        writer.flush();
        writer.close();
    }
    
    private final Map<String, String[]> processMultipartContent(
            Map<String, FileStatus> files) throws Exception {
        String[] values;
        String value;
        FileStatus filestatus;
        Map<String, String[]> parameters;
        
        parameters = new HashMap<>();
        for (String fkey : files.keySet()) {
            filestatus = files.get(fkey);
            if (!filestatus.item.isFormField()) {
                parameters.put(fkey, new String[] {filestatus.item.getName()});
                filestatus.file = true;
                continue;
            }
            
            value = filestatus.item.getString("UTF-8");
            values = parameters.get(fkey);
            if ((values == null) || (values != null && value.length() > 0))
                parameters.put(fkey, new String[] {value});
        }
        
        return parameters;
    }
    
    private final void run(
            HttpServletRequest req, HttpServletResponse resp, boolean reload)
                    throws Exception {
        Map<String, FileStatus> files = new HashMap<>();
        String sessionid = req.getSession().getId();
        ApplicationContext context = application.getContext();
        ContextEntry ctxentry = context.ctxentries.get(sessionid);
        Map<String, String[]> parameters = extract(files, req);
        String action = GetString.run(parameters , "action");
        
        if (ctxentry == null) {
            context.ctxentries.put(
                sessionid, ctxentry = new ContextEntry(application.context()));
            if (action == null)
                action = "connect";
        }
        
        try {
            req.setCharacterEncoding("UTF-8");
            ctxentry.action = action;
            ctxentry.sessionid = sessionid;
            ctxentry.locale = new Locale("pt", "BR");
            ctxentry.reload = reload;
            ctxentry.pageready = !(ctxentry.pagestack.size() == 0);
            if (ctxentry.pagectx != null)
                ProcessInput.run(ctxentry, parameters, files);
            if (ctxentry.action != null)
                ctxentry.output = application.run(ctxentry);
            print(resp, ctxentry.output);
        } catch (Exception e) {
            context.ctxentries.remove(sessionid);
            throw e;
        }
    }
    
    protected final void set(Connector connector) {
    	application.set(connector);
    }
    
//    private final void runAction(WebContextEntry ctxentry) throws Exception {
//        PageContext pagectx;
//        TypeContextEntry outputtype;
//        DataContextEntry output;
//        CauldronProgram program;
//        CauldronExecutionData execdata;
//        Handler handler = ctxentry.pagectx.getHandler(ctxentry.action);
//        
//        if (handler == null)
//            throw new CauldronException(
//                    "unregistered action %s.", ctxentry.action);
//        
//        program = ctxentry.programs.get(ctxentry.action);
//        if (program == null)
//            ctxentry.programs.put(
//                ctxentry.action, program = createProgram(ctxentry, handler));
//    
//        output = (program == null)? call(ctxentry, handler) :
//            program.execute(
//                    ctxentry.context, execdata = new CauldronExecutionData());
//        if (output != null) {
//            if ((outputtype = output.getType()).isAutoGenKey())
//                generateKey(ctxentry, output);
//            ctxentry.dbsession.save(output);
//        }
//        
//        if ((ctxentry.pagenav = ctxentry.context.getPageNavigation()) != 0) {
//            navigation[ctxentry.pagenav - 1].execute(ctxentry);
//            ctxentry.context.setPageName(ctxentry.pagestack.peek());
//        }
////      pagectx = ctxentry.context.getPage();
////      ctxentry.pageready = (ctxentry.pagectx == pagectx);
////      ctxentry.pagectx = pagectx;
//    }
//    
//    private final void setPageContextData(WebContextEntry ctxentry) {
//        DataContextEntry pagectx = ctxentry.context.datactx().get("pagectx");
//        pagectx.set("action", ctxentry.action);
//    }
//    
//  @Override
//  protected ContextEntry instance(Object[] args) {
//      WebContextEntry ctxentry;
//      Context context = context();
//
////      ctxsetup.accept(context);
////      if (args[3] != null)
////          context.setPageName("exhandler");
////      if (!(boolean)args[2])
////          throw new CauldronException("Invalid session. Restarting.");
//    ctxentry = new WebContextEntry(context);
//    ctxentry.ex = (Exception)args[3];
//    return ctxentry;
//  }
    
//  private final void run(ContextEntry _ctxentry) throws Exception {
//      WebContextEntry ctxentry = (WebContextEntry)_ctxentry;
//          
//      ctxentry.req = (HttpServletRequest)ctxentry.args[0];
//      ctxentry.resp = (HttpServletResponse)ctxentry.args[1];
//      ctxentry.reload = (boolean)ctxentry.args[2];
//      ctxentry.parameters = extract(ctxentry.files, ctxentry.req);
//      ctxentry.action = getString(ctxentry.parameters , "action");
//      ctxentry.locale = new Locale("pt", "BR");
//      setPageContextData(ctxentry);
//      processInput(ctxentry);
//      if (ctxentry.action != null)
//          runAction(ctxentry);
//      if ((ctxentry.pagenav == 0) && !ctxentry.pageready)
//          ctxentry.pagestack.push(ctxentry.context.getPageName());
//      print(ctxentry.resp, getContent(ctxentry));
//  }
//    private final DataContextEntry call(ContextEntry ctxentry, Handler handler)
//            throws Exception {
//        FunctionCall fnccall;
//        TypeContextEntry outputtype;
//        FunctionParameters taskdata = new FunctionParameters();
//        FunctionDetails details = ctxentry.context.
//            getFacility(handler.facility).getFunctionDetails(handler.function);
//        Map<String, String> input = details.getInputs();
//        
//        for (String ikey : input.keySet())
//            taskdata.add(ikey, getInputValue(ctxentry, input.get(ikey)));
//        outputtype = (details.output == null)?
//                null : ctxentry.context.typectx().get(details.output);
//        taskdata.output = (outputtype == null)?
//            null : ctxentry.context.datactx().instance(outputtype, "output");
//        taskdata.compile(ctxentry.context);
//        
//        fnccall = new FunctionCall(handler.function, taskdata);
//        return (DataContextEntry)ctxentry.context.
//                call(handler.facility, fnccall);
//    }
//    
//    private final String getInputValue(ContextEntry ctxentry, String arg) {
//        Set<String> entries = ctxentry.context.datactx().getReferences(arg);
//        for (String entry : entries)
//            return entry;
//        return null;
//    }
//    
//    private static final String directDetermination(
//            ContextEntry ctxentry, Handler handler) {
//        return ctxentry.context.getFacility(handler.facility).
//                getFunctionDetails(handler.function).output;
//        
//    }
//    
//    private static final String targetDetermination(
//            ContextEntry ctxentry, Handler handler) {
//        return handler.function;
//    }
}
