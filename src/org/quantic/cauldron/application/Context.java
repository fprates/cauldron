package org.quantic.cauldron.application;

import java.util.Set;

import org.quantic.cauldron.datamodel.DataContext;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.datamodel.TypeContext;
import org.quantic.cauldron.facility.Facility;
import org.quantic.cauldron.facility.FunctionCall;

public interface Context {
    
    public abstract void back();
    
    public abstract DataContextEntry call(String facility, FunctionCall fnccall)
            throws Exception;
    
    public abstract DataContext datactx();
    
    public abstract FunctionCall function(String name, Object parameters);
    
    public abstract <C extends Context> C getContext(String factory);
    
    public abstract Set<String> getFacilities();
    
    public abstract Facility getFacility(String name);
    
    public abstract String getPage();
    
    public abstract byte getPageNavigation();
    
    public abstract void home();
    
    public abstract void log(Exception e);
    
    public abstract void log(String text, Object... args);
    
    public abstract void reset();
    
    public abstract void setLogTarget(String facility, String function);
    
    public abstract void setPage(String name);
    
    public abstract TypeContext typectx();
}
