package org.quantic.cauldron.application;

import org.quantic.cauldron.application.target.Target;
import org.quantic.cauldron.datamodel.DataContext;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.trace.SignalData;

public class TestContext {
    public Target target;
    public DataContextEntry data;
    public SignalData signal;
    public DataContext datactx;
}
