package org.quantic.cauldron.application;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.quantic.cauldron.facility.Facility;
import org.quantic.cauldron.facility.FunctionReturn;
import org.quantic.cauldron.pagectx.PageContext;
import org.quantic.cauldron.trace.CauldronProgram;

public class ContextEntry {
    public String sessionid, action, output, currentaction, currentform;
    public PageContext pagectx;
    public Context context;
    public int logid;
    public long sequence;
    public boolean pageready, reload, onsignal, targetcontinue;
    public byte pagenav;
    public FunctionReturn fncret;
    public Stack<String> pagestack, targetstack;
    public DBSession dbsession;
    public Locale locale;
    public Object[] args;
    public Map<String, CauldronProgram> programs;
    public Facility sessionfac;
    public Set<String> targetsok;
    private BuildApplication buildapp;
    
    public ContextEntry(Context context) {
        pagestack = new Stack<>();
        programs = new HashMap<>();
        targetsok = new HashSet<>();
        targetstack = new Stack<>();
        this.context = context;
    }
    
    public final BuildApplication buildapp(String appname) {
        if (buildapp != null)
            return buildapp;
        return buildapp =
                new BuildApplication(this, new SharedContext(), appname);
    }
}
