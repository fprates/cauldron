package org.quantic.cauldron.application.console;

import java.util.function.Consumer;

import org.quantic.cauldron.application.Context;

public interface ContextSetup extends Consumer<Context> { }
