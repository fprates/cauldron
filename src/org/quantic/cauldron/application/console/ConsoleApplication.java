package org.quantic.cauldron.application.console;

import org.quantic.cauldron.application.AbstractApplication;
import org.quantic.cauldron.application.CauldronRenderer;
import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.application.SharedContext;
import org.quantic.cauldron.output.GenericFactory;
import org.quantic.cauldron.output.ToolFactory;
import org.quantic.cauldron.pagectx.PageSpecItem.TYPES;
import org.quantic.cauldron.pagectx.ToolData;

public class ConsoleApplication extends AbstractApplication {
    private int level;
    
    public ConsoleApplication(String name) {
        super(new SharedContext(), name);
        put(TYPES.FORM, new GenericFactory("form"));
        set((CauldronRenderer)(c)->renderer(c));
    }
    
    private final String build(ContextEntry ctxentry, ToolData data)
            throws Exception {
        ToolFactory factory;
        StringBuilder sb = new StringBuilder();
        
        factory = getFactory(data.type.getType());
        if (factory == null)
            return null;
        
        factory.execute(ctxentry, data, null);
        level++;
        for (int i = 0; i < level; i++)
            sb.append("-");
        sb.append("[");
        for (String name : data.attributes.keySet())
            sb.append(name).append("=").append(data.attributes.get(name)).
                    append(";");
        sb.append("]\n");
        for (String key : data.items.keySet())
            sb.append(build(ctxentry, data.items.get(key)));
        level--;
        return sb.toString();
    }
    
    private final String renderer(ContextEntry ctxentry) throws Exception {
        ToolData data = ctxentry.pagectx.instance(
                ctxentry.pagectx.getResponseComponent());
        StringBuilder sb = new StringBuilder();
        
        level = 0;
        for (String key : data.items.keySet())
            sb.append(build(ctxentry, data.items.get(key)).toString());
        return sb.toString();
    }
}

