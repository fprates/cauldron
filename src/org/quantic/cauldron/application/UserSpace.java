package org.quantic.cauldron.application;

public interface UserSpace {
    
    public abstract void run(ContextEntry ctxentry) throws Exception;
    
}
