package org.quantic.cauldron.application;

import java.util.List;

import org.quantic.cauldron.application.target.Target;
import org.quantic.cauldron.facility.Facility;
import org.quantic.cauldron.trace.CauldronProgram;
import org.quantic.cauldron.trace.ContextPool;
import org.quantic.cauldron.trace.ExecuteContext;
import org.quantic.cauldron.trace.SignalData;
import org.quantic.cauldron.trace.Trace;

public class BuildApplication extends AbstractApplication {
    private ExecuteContext execctx;
    private ContextEntry ctxentry;
    
    public BuildApplication(
            ContextEntry ctxentry, SharedContext sharedctx, String name) {
        super(new ApplicationContext(sharedctx, name), sharedctx, name);
        this.ctxentry = ctxentry;
        
        /*
         * program generation parameters
         */
        execctx = new ExecuteContext();
        execctx.facility = getName();
        execctx.extfac = new StringBuilder("ext_functions_").
                append(ctxentry.sessionid).toString();
        execctx.application = this;
    }
    
    @Override
    protected void dbinit(Context context) { }
    
    public final List<CauldronProgram> execute(
            Target target, SignalData signal, ApplicationInit appinit)
            throws Exception {
        List<CauldronProgram> success;
        Facility sessionfac;

        
        /*
         * initializing realtime facilities
         */
        getContext().datactx().clear();
        init(appinit);
        execctx.ctxpool = new ContextPool();
        execctx.ctxpool.target = target;
        execctx.signal = signal;
        execctx.context = context();
        facility(execctx.extfac);
        sessionfac = facilitiesinit(this, execctx.context, ctxentry.sessionid);
        
        /*
         * generating programs
         */
        success = Trace.execute(execctx);
        
        /*
         * removing realtime facilities
         */
        rmfacility(execctx.extfac);
        rmfacility(sessionfac.getName());
        
        return success;
    }
}
