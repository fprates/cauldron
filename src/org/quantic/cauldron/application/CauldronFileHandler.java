package org.quantic.cauldron.application;

import java.util.Map;

public interface CauldronFileHandler {
    
    public abstract void run(
            ContextEntry ctxentry, Map<String, ?> files, String key);
}

