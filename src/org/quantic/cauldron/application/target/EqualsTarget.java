package org.quantic.cauldron.application.target;

public class EqualsTarget extends AbstractTarget {
    private TargetContext context;
    
    public EqualsTarget(String document) {
        context.returns = document;
        config(context);
    }

    @Override
    protected void config(TargetContext context) {
        if (this.context == null) {
            this.context = context;
            return;
        }
        context.rules.put("page_call", (c)->true);
        context.rule = (c)->c.data.getType().getFullName().
                equals(context.returns);
    }
}
