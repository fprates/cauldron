package org.quantic.cauldron.application.target;

import org.quantic.cauldron.application.TestContext;

public interface Rule {

    public abstract boolean test(TestContext context);
}
