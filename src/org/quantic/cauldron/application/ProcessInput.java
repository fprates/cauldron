package org.quantic.cauldron.application;

import java.util.HashMap;
import java.util.Map;

import org.quantic.cauldron.datamodel.DataContext;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.datamodel.TypeContextEntry;
import org.quantic.cauldron.output.DataConverter;
import org.quantic.cauldron.output.StandardConverter;

public class ProcessInput {
    private static Map<String, DataConverter> converters;
    private static CauldronFileHandler filehandler;
    
    static {
        converters = new HashMap<>();
        converters.put("boolean", (c, v)->StandardConverter.runBoolean(c, v));
        converters.put("byte", (c, v)->StandardConverter.runByte(c, v));
        converters.put("date", (c, v)->StandardConverter.runDate(c, v));
        converters.put("double", (c, v)->StandardConverter.runDouble(c, v));
        converters.put("float", (c, v)->StandardConverter.runFloat(c, v));
        converters.put("int", (c, v)->StandardConverter.runInt(c, v));
        converters.put("long", (c, v)->StandardConverter.runLong(c, v));
        converters.put("short", (c, v)->StandardConverter.runShort(c, v));
        converters.put("string", (c, v)->StandardConverter.runString(c, v));
        converters.put("time", (c, v)->StandardConverter.runTime(c, v));
    }
    
    public static final void run(ContextEntry ctxentry,
            Map<String, String[]> parameters, Map<String, ?> files)
                throws Exception {
        String value;
        DataContextEntry entry;
        DataContext datactx = ctxentry.context.datactx();
        
        for (String key : ctxentry.pagectx.getInputs()) {
            value = GetString.run(parameters, key);
            entry = datactx.get(key);
            if (entry == null)
                throw new CauldronException(
                        "no model defined for input %s.", key);
            entry.set(runConverter(ctxentry, entry.getType(), value));
            if (filehandler != null)
                filehandler.run(ctxentry, files, key);
        }
    }
    
    private static final Object runConverter(
            ContextEntry ctxentry, TypeContextEntry type, String value)
                    throws Exception {
        return converters.get(type.getName()).run(ctxentry, value);
    }
}
