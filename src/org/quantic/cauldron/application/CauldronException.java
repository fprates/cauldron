package org.quantic.cauldron.application;

public class CauldronException extends RuntimeException {
    private static final long serialVersionUID = -3393799052504772427L;

    public CauldronException(String format, Object... args) {
        super(String.format(format, args));
    }
}
