package org.quantic.cauldron.application;

public interface CauldronRenderer {
    
    public abstract String run(ContextEntry ctxentry) throws Exception;
    
}
