package org.quantic.cauldron.application;

import org.quantic.cauldron.application.target.Target;
import org.quantic.cauldron.facility.Facility;
import org.quantic.cauldron.facility.Function;
import org.quantic.cauldron.facility.FunctionBySession;
import org.quantic.cauldron.pagectx.CauldronPage;

public interface Application {

    public abstract void add(Function function);
    
    public abstract void add(FunctionBySession function);

    public abstract Context context();
    
    public abstract Facility facility(String name);
    
    public abstract Facility facility(Context context, String name);
    
    public abstract <C extends Context> C getContext();
    
    public abstract <C extends Context> C getContext(String facility);
    
    public abstract void init(ApplicationInit appinit);
    
    public abstract CauldronPage page(String name);
    
    public abstract String run(ContextEntry ctxentry) throws Exception;
    
    public abstract void set(Connector connector);
    
    public abstract void set(CauldronRenderer renderer);
    
    public abstract void target(String action, String document);
    
    public abstract void target(String action, Target target);
}
