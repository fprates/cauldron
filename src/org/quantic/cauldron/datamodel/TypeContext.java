package org.quantic.cauldron.datamodel;

import java.util.Set;

import org.quantic.cauldron.application.SharedContext;

public class TypeContext {
    private SharedContext sharedctx;
    private SharedTypeContext shtypectx;
    
    public TypeContext(SharedContext sharedctx, String facility) {
        this.sharedctx = sharedctx;
        shtypectx = new SharedTypeContext();
        shtypectx.facility = facility;
        define(null, "?");
        define(get("?"), "any", null);
        define(get("any"), "boolean", null);
        define(get("any"), "double", null);
        define(get("any"), "int", null);
        define(get("any"), "long", null);
        define(get("any"), "string", null);
        define(get("any"), "numop", null);
        define(get("any"), "date", null);
        define(get("any"), "time", null);
        define(get("any"), "type", null);
    }
    
    private final void catalog(TypeContextEntry type) {
        String name = type.getName();
        String key = new StringBuilder(shtypectx.facility).
                append(".").append(name).toString();
        sharedctx.types.put(key, new String[] {shtypectx.facility, name});
    }
    
    public final TypeContextEntry define(
    		TypeContextEntry parent, String name, String key) {
        TypeContextEntry entry = new TypeContextEntry(
                shtypectx, parent, name, key);
        shtypectx.types.put(name, entry);
        if ((sharedctx != null) && (name != null))
            catalog(entry);
        return entry;
    }
    
    public final TypeContextEntry define(String name) {
    	return define((TypeContextEntry)null, name, null);
    }
    
    public final TypeContextEntry define(String name, String key) {
    	return define((TypeContextEntry)null, name, key);
    }
    
    public final TypeContextEntry get(String name) {
        String[] position;
        TypeContextEntry type = shtypectx.types.get(name);
        if (type != null)
            return type;
        if (sharedctx == null)
            return null;
        position = sharedctx.types.get(name);
        if (position == null)
            return null;
        return sharedctx.get(position[0]).getContext().typectx().
                get(position[1]);
    }
    
    public final Set<String> getTypes() {
        return sharedctx.types.keySet();
    }
}
