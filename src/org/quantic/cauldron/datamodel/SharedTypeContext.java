package org.quantic.cauldron.datamodel;

import java.util.HashMap;
import java.util.Map;

public class SharedTypeContext {
    public Map<String, TypeContextEntry> types;
    public String facility;
    
    public SharedTypeContext() {
        types = new HashMap<>();
    }
}
